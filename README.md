# SisHora

O Sishora é um Sistema de Gerenciamento de Atividades Complementares que visa
diminuir a quantidade de papel e facilitar os calculos das horas copmlementares,
onde os alunos cadastram suas atividades, o sistema calcula suas horas de acordo
com a grade e o eixo que o aluno e a atividade estão cadastrados e caso a atividade
seja aceita, essas horas serão adicionadas a quantidade total das horas dos alunos.
Quando as horas do aluno estão completas, ele passa a aparecer na lista das turmas
concludentes, onde o coordenador poderá selecionar a lista de alunos para emitir o
relatório (MPDF ou TFPDF recomendados pela facilidade de uso).

## Instalação

Para efetuar a instalação do projeto, é necessário:

* git
* composer
* nodejs
* php > 7

Efetuar o comando:
```
git clone https://mateusbds@bitbucket.org/mateusbds/sishora.git
```
Depois:
```
composer install
```
E por ultimo:
```
npm install
```

Após a instalação das dependências, é preciso criar o arquivo .env(copiar e colar o .env.example
e re nomear para .env), depois é necessário gerar o hash da aplicação com o seguinte comando:

```
php artisan key:generate
```

Após gerada, é necessário criar o banco de dados, basta importar o arquivo sql dentro do projeto.
Depois é preciso configurar o banco de dados no arquivo .env. Após a configuração do banco de dados,
é preciso criar um administrador para o sistema. O usuário pode ser criado com o tinker do laravel:

```
php artisan tinker
```

Após o comando, o console vai entrar no CLI do laravel, basta programar normalmente como se estivesse
programando no editor de texto:

```
$user = [
    'nome' => 'Nome Do Usuário',
    'matricula' => 'Matricula',
    'email' => 'emaildousuario@email.com',
    'password' => 'senha',
    'profile_id' => '1' //Antes de cadastrar o usuário, é preciso cadastrar os perfis (1 - admin, 2 coord, - 3 aluno)
];

$user['password'] = Illuminate\Support\Facades\Hash::make($user['password']);

App\Models\User::create($user);
```

Feito isso, já é possivel entrar no sistema. Só lembrando que a primeira coisa a ser feita após a criação do usuário, é a criação de um curso, ou ocorrerar erros durante o uso do sistema.

## Todos

A inteligência do sistema no geral, cálculo de cada atividade de acordo com os arquivos e com os dados que os coordenadores repassam. Cada curso tem uma maneira diferente de calcular as horas das atividades. Provavelmente existem alguns outros todos no sistema que precisarão ser implementados de acordo com o desenvolvimento da inteligência do sistema.