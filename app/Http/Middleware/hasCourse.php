<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Course;
use Illuminate\Support\Facades\Auth;

class hasCourse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()['profile_id']  != 3) {
            $course = Course::select()->where('user_id', '=', Auth::id())->get()->toArray();
        }

        if($course == null)
            return redirect()->route('course')->with('message', 'O usuário precisa gerenciar um curso! Contacte o administrador do sistema.');

        return $next($request);
    }
}
