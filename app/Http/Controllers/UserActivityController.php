<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserActivity;
use Illuminate\Http\Request;
use App\Models\GradesActivity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/**
 * @todo Checagem em cascata, só pode deletar caso uma tabela não possua nenhuma chave estrangeira
 * cadastrada.
 * @todo Notificações para quando a atividade do aluno for validada/recusada.
 */
class UserActivityController extends Controller
{

    private $fileStorage;

    public function __construct() {
        $this->fileStorage = (storage_path().'/app/Activities/');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userActivities = UserActivity::select()->get()->toArray();

        return view('userActivity.index', ['userActivities' => $userActivities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gradesActivities = GradesActivity::select('grades_activities.id as gradesActivitiesId',
            'activities.id as activitiesId', 'grades_activities.*', 'activities.*')
            ->join('activities', 'activities.id', '=', 'grades_activities.activity_id')
            ->where('grade_id', '=', Auth::user()['grades_id'])
            ->get()
            ->toArray();

        return view('userActivity.create', ['gradesActivities' => $gradesActivities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
        $validatedData = $request->validate([
            'descricao' => 'required',
            'carga_horaria' => 'required',
            'instituicao' => 'required',
            'arquivo' => 'required|file|mimes:pdf,doc,docx,jpg,jpeg,png,bmp',
            'grades_activity_id' => 'required'
        ]);

        $validatedData['user_id'] = Auth::user()['id'];

        if($request->file('arquivo')->isValid()) {
            $activity = UserActivity::create($validatedData);
            $file = $request->file('arquivo');

            $fileName = substr($file->getClientOriginalName(), 0,
                 strpos($file->getClientOriginalName(), "."));

            $actualName = $fileName.'_'.Auth::user()['id'].'_'.$activity['id'].'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('Activities', $actualName);

            $activity = UserActivity::find($activity['id']);
            $activity->arquivo = $actualName;
            $activity->save();
        }
        else {
            return redirect()->route('userActivity')->with('message', 'Arquivo inválido.');
        }
        return redirect()->route('userActivity')->with('message', 'Atividade enviada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userActivity = UserActivity::find($id);
        if($userActivity['validado'] == -1) {
            $gradesActivities = GradesActivity::select('grades_activities.id as gradesActivitiesId',
                'activities.id as activitiesId', 'grades_activities.*', 'activities.*')
                ->join('activities', 'activities.id', '=', 'grades_activities.activity_id')
                ->where('grade_id', '=', Auth::user()['grades_id'])
                ->get()
                ->toArray();

            return view('userActivity.edit', ['userActivity' => $userActivity, 'gradesActivities' => $gradesActivities]);
        }
        else {
            return redirect()->route('userActivity')->with('message', 'Você não pode editar uma atividade que já foi aprovada/recusada.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userActivity = UserActivity::find($id);

        if($userActivity['validado'] == -1) {
            $validatedData = $request->validate([
                'descricao' => 'required',
                'carga_horaria' => 'required',
                'instituicao' => 'required',
                'arquivo' => 'file|mimes:pdf,doc,docx,jpg,jpeg,png,bmp',
                'grades_activity_id' => 'required'
            ]);

            if(isset($validatedData['arquivo'])) {
                $userActivity->fill($validatedData);
                if($request->file('arquivo')->isValid()) {
                    $file = $request->file('arquivo');

                    $fileName = substr($file->getClientOriginalName(), 0,
                        strpos($file->getClientOriginalName(), "."));

                    $actualName = $fileName.'_'.Auth::user()['id'].'_'.$userActivity->id.'.'.$file->getClientOriginalExtension();
                    $path = $file->storeAs('Activities', $actualName);

                    $userActivity->arquivo = $actualName;
                    $userActivity->save();
                }
                else {
                    return redirect()->route('userActivity')->with('message', 'Arquivo inválido.');
                }
            }
            else  {
                $userActivity->fill($validatedData);
                $userActivity->save();
            }

            return redirect()->route('userActivity')->with('message', 'Atividade editada.');
        }
        else {
            return redirect()->route('userActivity')->with('message', 'Você não pode editar uma atividade que já foi validada/recusada.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userActivity = UserActivity::find($id);

        if($userActivity == -1) {
            $arquivo = $userActivity->arquivo;

            if(UserActivity::destroy($id) != null)
                unlink(storage_path().'/app/Activities/'.$arquivo);
            
            return redirect()->route('userActivity')->with('message', 'Atividade apagada.');
        }
        else {
            return redirect()->route('userActivity')->with('message', 'Você não pode apagar uma atividade que já foi validada/recusada.');
        }
    }

    public function profile() 
    {
        $user = User::find(Auth::id());

        if($user->avatar == null) {
            $picture = Storage::url('public/profile_pictures/gravatar.jpg');
        }
        else {
            $picture = Storage::url('public/profile_pictures/'.$user->avatar);
        }
        
        return view('userActivity.profile', ['picture' => $picture, 'user' => $user]);
    }

    public function profileSave(Request $request) 
    {
        $user = User::find(Auth::id());
        if($request->password || $request->password_confirmation || $request->password_antigo) {
            $validatedData = $request->validate([
                'password_antigo' => 'required',
                'password' => 'required|confirmed|min:6',
                'nome' => 'required|min:5',
                'matricula' => 'required|numeric'
            ]);

            if(Hash::check($validatedData['password_antigo'], $user->password)) {
                $validatedData['password'] = Hash::make($validatedData['password']);
                $user->fill($validatedData);
                $user->save();
                return redirect()->route('sProfile')->with('message', 'Edições salvas.');
            }
            else {
                return redirect()->route('sProfile')->with('message', 'Senha digitada não corresponde a senha atual.');
            }
        }

        $validatedData = $request->validate([
            'nome' => 'required|min:5',
            'matricula' => 'required|numeric'
        ]);

        $user->fill($validatedData);
        $user->save();
        
        return redirect()->route('sProfile')->with('message', 'Edições salvas.');
    }

    /**
     * Logout
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::logout();

        return redirect()->route('login')->with('message', 'Você saiu.');
    }

    /**
     * Download do comprovante que o aluno upou
     * 
     * @todo Só pode deletar atividade que estão pendentes
     *  validado = 0 pendente
     *  validado = 1 aprovada
     *  validado = 2 recusada
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function download($id)
    {
        $activity = UserActivity::find($id);

        $header = [
            'Content-type' => 'application/pdf'
        ];

        return response()->file($this->fileStorage.$activity->arquivo, $header);
    }
}
