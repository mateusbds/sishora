<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Course;
use App\Models\UserActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index()
    {
        $user = Auth::user()->toArray();

        $course = Course::select()->where('user_id', '=', Auth::id())->get()->toArray();

        $course = array_shift($course);

        $activities = UserActivity::select(
            'users_activities.*',
            'users_activities.descricao as uaDesc',
            'users_activities.id as uaID',
            'users.id as userID',
            'users.nome as userNome',
            'users.*',
            'grades_activities.id as gaID',
            'grades_activities.*',
            'grades.id as gID',
            'grades.nome as gNome',
            'grades.*',
            'activities.id as activityID',
            'activities.nome as activityNome'
        )
        ->join('users', 'users.id', '=', 'users_activities.user_id')
        ->join('grades_activities', 'grades_activities.id', '=', 'users_activities.grades_activity_id')
        ->join('grades', 'grades.id', '=', 'grades_activities.grade_id')
        ->join('activities', 'activities.id', '=', 'grades_activities.activity_id')
        ->where('grades.course_id', '=', $course['id'])
        ->whereNull('users_activities.validado')
        ->get()
        ->toArray();

        return view('user.index', ['activities' => $activities]);
    }

    /**
     * Função de login, serve para o get e para o post
     * 
     * @param Request $request
     * @return void
     */
    public function login(Request $request)
    {
        $validatedData = $request->validate([
            'matricula' => 'required',
            'password' => 'required'
        ]);

        if(Auth::check()) {
            if(Auth::user()['profile_id'] != 3) {
                return redirect()->intended(action('UserController@index'));
            }
            else {
                return redirect()->intended(action('UserActivityController@index'));
            }
        }
        else {
            if(Auth::attempt($validatedData)) {
                if(Auth::user()['profile_id'] != 3) {
                    return redirect()->intended(action('UserController@index'));
                }
                else {
                    return redirect()->intended(action('UserActivityController@index'));
                }
            }
        }

        return redirect()->back()->withErrors('Login ou Senha inválido(s).');
    }

    public function loginForm()
    {
        return view('user.login');
    }

    public function users()
    {
        $users = User::select('profiles.id as profileId', 'profiles.nome as profileName', 'users.*')
            ->join('profiles', 'profiles.id', '=', 'users.profile_id')
            ->whereNotIn('users.id', [Auth::user()['id']])->get()->toArray();

        return view('user.users', ['users' => $users]);
    }

    public function edit($id)
    {
        $user = User::find('1');

        return view('user.edit', ['user' => $user]);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $validatedData = $request->validate([
            'matricula' => 'required',
            'nome' => 'required',
            'email' =>  'required|email',
            'password' => 'required|min:6',
            'profile_id' => 'required'
        ]);

        if($validatedData['password'] != $user->password) {
            $user->password = Hash::make($validatedData['password']);
            echo $user->password;
        }

        $user->nome = $validatedData['nome'];
        $user->matricula = $validatedData['matricula'];
        $user->email = $validatedData['email'];
        $user->profile_id = $validatedData['profile_id'];

        $user->save();

        return redirect()->route('users')->with('message', 'Usuário editado com sucesso.');
    }

    public function delete($id)
    {
        User::destroy($id);
        
        return redirect()->route('users')->with('message', 'Usuário deletado com sucesso.');
    }

    public function create()
    {
        return view('user.create');
    }

    /**
     * Action onde o coordenador verá uma lista de atividades para validar ou recusar.
     *
     * @return \Illuminate\Http\Response
     */
    public function activities() 
    {
        $course = Course::select()->where('user_id', '=', Auth::id())->get()->toArray();

        $course = array_shift($course);

        $activities = UserActivity::select(
                'users_activities.*',
                'users_activities.descricao as uaDesc',
                'users_activities.id as uaID',
                'users.id as userID',
                'users.nome as userNome',
                'users.*',
                'grades_activities.id as gaID',
                'grades_activities.*',
                'grades.id as gID',
                'grades.nome as gNome',
                'grades.*',
                'activities.id as activityID',
                'activities.nome as activityNome'
            )
            ->join('users', 'users.id', '=', 'users_activities.user_id')
            ->join('grades_activities', 'grades_activities.id', '=', 'users_activities.grades_activity_id')
            ->join('grades', 'grades.id', '=', 'grades_activities.grade_id')
            ->join('activities', 'activities.id', '=', 'grades_activities.activity_id')
            ->where('grades.course_id', '=', $course['id'])
            ->whereNull('users_activities.validado')
            ->get()
            ->toArray(); 

        return view('user.activity.activity', ['activities' => $activities]);
    }

    /**
     * Action onde o coordenador pode validar ou recusar atividades dos alunos.
     *
     * @return \Illuminate\Http\Response
     */
    public function activityView($id) 
    {
        $course = Course::select()->where('user_id', '=', Auth::id())->get()->toArray();

        $course = array_shift($course);

        $activity = UserActivity::select(
                'users_activities.*',
                'users_activities.descricao as uaDesc',
                'users_activities.id as uaID',
                'users.id as userID',
                'users.nome as userNome',
                'users.*',
                'grades_activities.id as gaID',
                'grades_activities.*',
                'grades.id as gID',
                'grades.nome as gNome',
                'grades.*',
                'activities.id as activityID',
                'activities.nome as activityNome'
            )
            ->join('users', 'users.id', '=', 'users_activities.user_id')
            ->join('grades_activities', 'grades_activities.id', '=', 'users_activities.grades_activity_id')
            ->join('grades', 'grades.id', '=', 'grades_activities.grade_id')
            ->join('activities', 'activities.id', '=', 'grades_activities.activity_id')
            ->where([
                ['grades.course_id', '=', $course['id']],
                ['users_activities.id', '=', $id],
                ])
            ->get()
            ->toArray();

        $activity = array_shift($activity);

        unset($activity['password']);

        return view('user.activity.view', ['activity' => $activity]);
    }

    /**
     * Baixa o arquivo da atividade.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewPDF($id, $name)
    {
        $file = Storage::download("Activities/".$name);
        return $file;
    }

    /**
     * Action caso recuse a atividade.
     *
     * @return \Illuminate\Http\Response
     */
    public function refuse($id)
    {
        $activity = UserActivity::find($id);

        if(!isset($activity['validado'])) {
            $activity['validado'] = 0;

            $activity->save();

            return redirect()->route('userAct')->with('message', 'Atividade recusada.');
        }
        
        return redirect()->route('userAct')->with('message', 'Não é possivel editar o status dessa atividade.');
    }

    /**
     * Action caso aceite a atividade.
     *
     * @return \Illuminate\Http\Response
     */
    public function accept($id)
    {
        $activity = UserActivity::find($id);

        if(!isset($activity['validado'])) {
            $activity['validado'] = 1;

            $activity->save();

            return redirect()->route('userAct')->with('message', 'Atividade validada.');
        }

        return redirect()->route('userAct')->with('message', 'Não é possivel editar o status dessa atividade.');
    }

    public function profile() 
    {
        $user = User::find(Auth::id());

        if($user->avatar == null) {
            $picture = Storage::url('public/profile_pictures/gravatar.jpg');
        }
        else {
            $picture = Storage::url('public/profile_pictures/'.$user->avatar);
        }

        //echo $picture;die;

        return view('user.profile', ['picture' => $picture, 'user' => $user]);
    }

    public function profileSave(Request $request) 
    {
        $user = User::find(Auth::id());
        if($request->password || $request->password_confirmation || $request->password_antigo) {
            $validatedData = $request->validate([
                'password_antigo' => 'required',
                'password' => 'required|confirmed|min:6',
                'nome' => 'required|min:5',
                'matricula' => 'required|numeric'
            ]);

            if(Hash::check($validatedData['password_antigo'], $user->password)) {
                $validatedData['password'] = Hash::make($validatedData['password']);
                $user->fill($validatedData);
                $user->save();
                return redirect()->route('acProfile')->with('message', 'Edições salvas.');
            }
            else {
                return redirect()->route('acProfile')->with('message', 'Senha digitada não corresponde a senha atual.');
            }
        }

        $validatedData = $request->validate([
            'nome' => 'required|min:5',
            'matricula' => 'required|numeric'
        ]);

        $user->fill($validatedData);
        $user->save();
        
        return redirect()->route('acProfile')->with('message', 'Edições salvas.');
    }

    public function changePic(Request $request)
    {
        $fileName;
        if($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
            $file = $request->file('avatar');

            $fileName = md5(uniqid(rand(), true));
            $fileName .= '.'.$file->getClientOriginalExtension();

            $path = $file->storeAs('public/profile_pictures', $fileName);

            $user = User::find(Auth::id());
            $user->avatar = $fileName;
            $user->save();

            return response()->json(['msg' => 'Success']);
        }

        return response()->json(['error']);
    }

    /**
     * Logout de usuário.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::logout();

        return redirect()->route('login')->with('message', 'Você saiu.');
    }
}
