<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Grade;
use App\Models\Course;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grades = Grade::select('grades.id', 'grades.horas', 'grades.status',  'grades.nome as gradesNome', 'courses.nome as coursesName')
            ->join('courses', 'courses.id', '=', 'grades.course_id')
            ->get()
            ->toArray();

        return view('grade.index', ['grades' => $grades]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::select()->get()->toArray();

        return view('grade.create', ['courses' => $courses]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nome' => 'required',
            'horas' => 'required',
            'status' => 'nullable',
            'course_id' => 'required'
        ]);

        $validatedData['status'] = $validatedData['status'] == 'on' ? 1 : 0;

        Grade::create($validatedData);
        
        return redirect()->route('grade')->with('message', 'Grade criada com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grade = Grade::find($id);
        $courses = Course::select()->get()->toArray();

        return view('grade.edit', ['grade' => $grade, 'courses' => $courses]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $grade = Grade::find($id);

        $validatedData = $request->validate([
            'nome' => 'required',
            'horas' => 'required',
            'status' => 'nullable',
            'course_id' => 'required'
        ]);

        $validatedData['status'] = $validatedData['status'] == 'on' ? 1 : 0;

        $grade->nome = $validatedData['nome'];
        $grade->horas = $validatedData['horas'];
        $grade->status = $validatedData['status'];
        $grade->course_id = $validatedData['course_id'];

        $grade->save();

        return redirect()->route('grade')->with('message', 'Grade editada com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Grade::destroy($id);

        return redirect()->route('grade')->with('message', 'Grade deletada com sucesso.');
    }
}
