<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Actuation;

class ActuationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actuations = Actuation::select()->get()->toArray();

        return view('actuation.index', ['actuations' => $actuations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('actuation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nome' => 'required'
        ]);

        Actuation::create($validatedData);

        return redirect()->route('actuation')->with('message', 'Eixo de Atuação criado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $actuation = Actuation::find($id);

        return view('actuation.edit', ['actuation' => $actuation]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $actuation = Actuation::find($id);

        $validatedData = $request->validate([
            'nome' => 'required'
        ]);

        $actuation->nome = $validatedData['nome'];

        $actuation->save();

        return redirect()->route('actuation')->with('message', 'Eixo de Atuação editado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Actuation::destroy($id);

        return redirect()->route('actuation')->with('message', 'Eixo de Atuação deletado com sucesso.');
    }
}
