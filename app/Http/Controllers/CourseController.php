<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::select('cod_curso', 'courses.nome as courseNome', 'courses.id as courseID', 'users.id as userID', 'users.nome as userNome')
            ->join('users', 'users.id', '=', 'courses.user_id')
            ->get()
            ->toArray();

        return view('course.index', ['courses' => $courses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user(['profile_id'] == 2)) {
            return back();
        }

        $users = User::select()->where('profile_id', 2)->get()->toArray();

        return view('course.create', ['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nome' => 'required',
            'cod_curso' => 'required',
            'user_id' => 'required'
        ]);

        $course = Course::create($validatedData);

        return redirect()->route('course')->with('message', 'Curso criado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user(['profile_id'] == 2)) {
            return back();
        }
        $users = User::select()->where('profile_id', 2)->get()->toArray();
        $course = Course::find($id);

        return view('course.edit', ['course' => $course, 'users' => $users]);
    }   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = Course::find($id);

        $validatedData = $request->validate([
            'nome' => 'required',
            'cod_curso' => 'required',
            'user_id' => 'required'
        ]);

        $course->nome = $validatedData['nome'];
        $course->cod_curso = $validatedData['cod_curso'];
        $course->user_id = $validatedData['user_id'];

        $course->save();

        return redirect()->route('course')->with('message', 'Curso editado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Course::destroy($id);
        
        return redirect()->route('course')->with('message', 'Curso deletado com sucesso.');
    }
}
