<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GradesActivity;
use App\Models\Grade;
use App\Models\Course;
use App\Models\Activity;
use App\Models\Actuation;
use Illuminate\Support\Facades\Auth;

class GradesActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $gradesActivities = GradesActivity::select('*',
            'grades_activities.id as gaId',
            'grades.id as gradeId',
            'activities.id as activityId',
            'actuations.id as actuationId',
            'grades.nome as gradeNome',
            'activities.nome as activityNome',
            'actuations.nome as actuationNome')
            ->join('grades', 'grades.id', '=', 'grade_id')
            ->join('activities', 'activities.id', '=', 'activity_id')
            ->join('actuations', 'actuations.id', '=', 'actuation_id')
            ->where('grade_id', '=', $id)
            ->get()
            ->toArray();
        
        $grade = Grade::find($id);

        return view('gradesActivity.index', ['gradesActivities' => $gradesActivities, 'grade' => $grade]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if(Auth::user()['profile_id'] == 2) {
            $course = Course::select()
                        ->where('user_id', '=', Auth::user()['id'])
                        ->get()
                        ->toArray();

            $grades = Grade::select()->where('course_id', '=', $course['id'])->get()->toArray();
        }
        else {
            $grades = Grade::select()->get()->toArray();            
        }

        $activities = Activity::select()->get()->toArray();

        $actuations = Actuation::select()->get()->toArray();

        $_grade = Grade::find($id);

        return view('gradesActivity.create', ['grades' => $grades, 'activities' => $activities, 'actuations' => $actuations, '_grade' => $_grade]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $validatedData = $request->validate([
            'activity_id' => 'required|numeric',
            'actuation_id' => 'required|numeric',
            'quantidade' => 'required',
            'limite' => 'required',
            'unidade' => 'required'
        ]);

        $validatedData['grade_id'] = $id;

        GradesActivity::create($validatedData);

        return redirect()->route('gradesActivity', ['id' => $id])->with('message', 'Atividade da Grade salva com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $activityId)
    {
        $gradesActivity = GradesActivity::find($activityId);

        if(Auth::user()['profile_id'] == 2) {
            $course = Course::select()
                        ->where('user_id', '=', Auth::user()['id'])
                        ->get()
                        ->toArray();

            $grades = Grade::select()->where('course_id', '=', $course['id'])->get()->toArray();
        }
        else {
            $grades = Grade::select()->get()->toArray();
        }
        
        $activities = Activity::select()->get()->toArray();
        $actuations = Actuation::select()->get()->toArray();
        $_grade = Grade::find($id);

        return view('gradesActivity.edit', ['gradesActivity' => $gradesActivity, 'grades' => $grades, 'activities' => $activities, 'actuations' => $actuations, '_grade' => $_grade]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $activityId)
    {
        $gradesActivity = GradesActivity::find($activityId);

        $validatedData = $request->validate([
            'activity_id' => 'required|numeric',
            'actuation_id' => 'required|numeric',
            'quantidade' => 'required',
            'limite' => 'required',
            'unidade' => 'required'
        ]);

        $validatedData['grade_id'] = $id;

        $gradesActivity->fill($validatedData);
        $gradesActivity->save();

        return redirect()->route('gradesActivity', ['id' => $id])->with('message', 'Atividade da Grade editada com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $activityId)
    {
        GradesActivity::destroy($activityId);

        return redirect()->route('gradesActivity', ['id' => $id])->with('message', 'Atividade da Grade deletada com sucesso.');
    }
}
