<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Actuation extends Model
{
    /**
     * Desativar timestamps
     *
     * @var boolean
     */
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
