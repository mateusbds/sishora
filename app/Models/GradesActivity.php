<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GradesActivity extends Model
{
    /**
     * Tabela
     *
     * @var string
     */
    protected $table = 'grades_activities';

    /**
     * Desativar timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'grade_id', 'activity_id', 'actuation_id', 'quantidade', 'limite', 'unidade'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
