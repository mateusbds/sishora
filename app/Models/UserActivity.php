<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    /**
     * Tabela
     *
     * @var string
     */
    protected $table = 'users_activities';

    /**
     * Desativar timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'descricao', 'carga_horaria', 'carga_aproveitada', 'instituicao', 'arquivo', 'enviado',
        'user_id', 'grades_activity_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
