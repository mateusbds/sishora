-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05-Jul-2018 às 02:10
-- Versão do servidor: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sishora`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `activities`
--

INSERT INTO `activities` (`id`, `nome`) VALUES
(2, 'Natação');

-- --------------------------------------------------------

--
-- Estrutura da tabela `actuations`
--

CREATE TABLE `actuations` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `actuations`
--

INSERT INTO `actuations` (`id`, `nome`) VALUES
(2, 'Pesquisa'),
(3, 'Extensão'),
(4, 'Ensino');

-- --------------------------------------------------------

--
-- Estrutura da tabela `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) COLLATE utf8_bin NOT NULL,
  `cod_curso` varchar(45) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `courses`
--

INSERT INTO `courses` (`id`, `nome`, `cod_curso`, `user_id`) VALUES
(2, 'Redes de Computadores', '123123', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_bin NOT NULL,
  `horas` varchar(4) COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) NOT NULL,
  `course_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `grades`
--

INSERT INTO `grades` (`id`, `nome`, `horas`, `status`, `course_id`) VALUES
(3, 'Grade 2018.1 de Redes de Computadores', '150', 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `grades_activities`
--

CREATE TABLE `grades_activities` (
  `id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `actuation_id` int(11) NOT NULL,
  `quantidade` float NOT NULL,
  `limite` float NOT NULL,
  `unidade` set('DIAS','HORAS','QUANTIDADE') COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `grades_activities`
--

INSERT INTO `grades_activities` (`id`, `grade_id`, `activity_id`, `actuation_id`, `quantidade`, `limite`, `unidade`) VALUES
(1, 3, 2, 3, 50, 100, 'HORAS');

-- --------------------------------------------------------

--
-- Estrutura da tabela `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `profiles`
--

INSERT INTO `profiles` (`id`, `nome`) VALUES
(1, 'Admin'),
(2, 'Coord'),
(3, 'Aluno');

-- --------------------------------------------------------

--
-- Estrutura da tabela `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `ano` varchar(4) COLLATE utf8_bin NOT NULL,
  `semestre` varchar(2) COLLATE utf8_bin NOT NULL,
  `course_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `teams`
--

INSERT INTO `teams` (`id`, `ano`, `semestre`, `course_id`) VALUES
(1, '2017', '2', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `matricula` varchar(15) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `nome` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(45) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ultimo_login` timestamp NULL DEFAULT NULL,
  `ultimo_request` timestamp NULL DEFAULT NULL,
  `profile_id` int(11) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `grades_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `matricula`, `password`, `nome`, `email`, `created_at`, `updated_at`, `ultimo_login`, `ultimo_request`, `profile_id`, `team_id`, `grades_id`) VALUES
(1, '123123', '$2y$10$aRCkoMtmVJSb54xJacYedeoRNnZoH/j93dio3dEgFSnQoQxqLdbS2', 'Mateus Barbosa da Silva', 'mateusbds222@gmail.com', '2018-05-29 23:35:21', '2018-05-30 02:35:21', NULL, NULL, 1, NULL, NULL),
(2, '20141108010023', '$2y$10$RqUIgphf1XIiSPz1njSOy.WgCTR3BOw6VXt5a4HQluzN.X4wEieT.', 'Raul Castro Santos', 'raulcs@gmail.com', '2018-05-30 00:59:48', '2018-05-30 00:59:48', NULL, NULL, 3, NULL, NULL),
(3, '123123123', '$2y$10$QOzU2SEQ..nA9HNYr1wlB.eWavjuvUTOl4yJuW/A5s9h9n2YSMXCK', 'Carlos Henrique Leitão Cavalcante', 'henriqueleitao@ifce.edu', '2018-06-01 22:25:08', '2018-06-01 22:25:08', NULL, NULL, 2, NULL, NULL),
(4, '20131103010098', '$2y$10$NitRwebdYLVO.sOmUIkhtO6JFtV6Ok53tNzZ.o/K5oj1fPDQjKD5u', 'Mateus Barbosa da Silva', 'mateusbds222@gmail.com', '2018-06-09 15:21:44', '2018-06-08 00:49:37', NULL, NULL, 3, NULL, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users_activities`
--

CREATE TABLE `users_activities` (
  `id` int(11) NOT NULL,
  `descricao` varchar(255) COLLATE utf8_bin NOT NULL,
  `carga_horaria` int(11) NOT NULL,
  `carga_aproveitada` int(11) DEFAULT NULL,
  `instituicao` varchar(100) COLLATE utf8_bin NOT NULL,
  `arquivo` varchar(100) COLLATE utf8_bin NOT NULL,
  `validado` tinyint(4) DEFAULT NULL,
  `enviado` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `grades_activity_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `users_activities`
--

INSERT INTO `users_activities` (`id`, `descricao`, `carga_horaria`, `carga_aproveitada`, `instituicao`, `arquivo`, `validado`, `enviado`, `user_id`, `grades_activity_id`) VALUES
(1, 'Natação', 12, NULL, 'IFCE', 'Mateus-Mai18_4_1.pdf', 1, NULL, 4, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `actuations`
--
ALTER TABLE `actuations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`,`user_id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades_activities`
--
ALTER TABLE `grades_activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_activities`
--
ALTER TABLE `users_activities`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `actuations`
--
ALTER TABLE `actuations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `grades_activities`
--
ALTER TABLE `grades_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users_activities`
--
ALTER TABLE `users_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
