<?php

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return redirect()->route('login');
});

//Login route
Route::get('/login', 'UserController@loginForm')->name('login')->middleware('checkAuthenticated');

Route::post('/login', 'UserController@login')->name('doLogin')->middleware('checkAuthenticated');

Route::post('/changePic', 'UserController@changePic')->middleware('auth');

//Users group
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin', 'course']], function() {
    Route::get('/', 'UserController@index')->name('user');
    Route::get('/user', 'UserController@users')->name('users');
    Route::get('/user/edit/{id}', 'UserController@edit');
    Route::get('/user/delete/{id}', 'UserController@delete');
    Route::get('/user/create', 'UserController@create');
    Route::post('/user/register', 'UserController@register')->name('register');
    Route::get('/user/activities', 'UserController@activities')->name('userAct');
    Route::get('/user/activities/view/{id}', 'UserController@activityView');
    Route::patch('/user/update/{id}', 'UserController@update');
    Route::get('/user/logout', 'UserController@logout')->name('/user/logout');
    Route::get('/user/activities/view/{id}/{pdf}', 'UserController@viewPDF')->name('pdf');
    Route::get('/user/activities/accept/{id}', 'UserController@accept');
    Route::get('/user/activities/refuse/{id}', 'UserController@refuse');
    Route::get('/profile', 'UserController@profile')->name('acProfile');
    Route::post('/profile', 'UserController@profileSave');
});

//Courses group
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function() {
    Route::get('/course', 'CourseController@index')->name('course');
    Route::get('/course/create', 'CourseController@create');
    Route::get('/course/edit/{id}', 'CourseController@edit');
    Route::get('/course/destroy/{id}', 'CourseController@destroy');
    Route::post('/course/store', 'CourseController@store');
    Route::patch('/course/update/{id}', 'CourseController@update');
});

//Activity group
Route::group(['prefix' => 'admin', 'middleware' =>['auth', 'admin', 'course']], function() {
    Route::get('/activity', 'ActivityController@index')->name('activity');
    Route::get('/activity/create', 'ActivityController@create');
    Route::get('/activity/edit/{id}', 'ActivityController@edit');
    Route::get('/activity/destroy/{id}', 'ActivityController@destroy');
    Route::post('/activity/store', 'ActivityController@store');
    Route::patch('/activity/update/{id}', 'ActivityController@update');
});

//Actuation group
Route::group(['prefix' => 'admin', 'middleware' =>['auth', 'admin', 'course']], function() {
    Route::get('/actuation', 'ActuationController@index')->name('actuation');
    Route::get('/actuation/create', 'ActuationController@create');
    Route::get('/actuation/edit/{id}', 'ActuationController@edit');
    Route::get('/actuation/destroy/{id}', 'ActuationController@destroy');
    Route::post('/actuation/store', 'ActuationController@store');
    Route::patch('/actuation/update/{id}', 'ActuationController@update');
});

//Grades group
Route::group(['prefix' => 'admin', 'middleware' =>['auth', 'admin', 'course']], function() {
    Route::get('/grade', 'GradeController@index')->name('grade');
    Route::get('/grade/create', 'GradeController@create');
    Route::get('/grade/edit/{id}', 'GradeController@edit');
    Route::get('/grade/destroy/{id}', 'GradeController@destroy');
    Route::post('/grade/store', 'GradeController@store');
    Route::patch('/grade/update/{id}', 'GradeController@update');
});

//gradesActivity group
Route::group(['prefix' => 'admin', 'middleware' =>['auth', 'admin', 'course']], function() {
    Route::get('/grade/{id}/gradesActivity/', 'GradesActivityController@index')->name('gradesActivity');
    Route::get('/grade/{id}/gradesActivity/create', 'GradesActivityController@create')->name('gradesActivity/create');
    Route::get('/grade/{id}/gradesActivity/edit/{activityId}', 'GradesActivityController@edit')->name('gradesActivity/edit');
    Route::get('/grade/{id}/gradesActivity/destroy/{activityId}', 'GradesActivityController@destroy')->name('gradesActivity/destroy');
    Route::post('/grade/{id}/gradesActivity/store', 'GradesActivityController@store')->name('gradesActivity/store');
    Route::patch('/grade/{id}/gradesActivity/update/{activityId}', 'GradesActivityController@update')->name('gradesActivity/update');
});

//teams
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin', 'course']], function() {
    Route::get('/team', 'TeamController@index')->name('team');
    Route::get('/team/create', 'TeamController@create');
    Route::post('/team/store', 'TeamController@store');
    Route::get('/team/edit/{id}', 'TeamController@edit');
    Route::get('/team/destroy/{id}', 'TeamController@destroy');
});

//userActivities group
Route::group(['prefix' => 'aluno', 'middleware' => ['auth', 'isStudent']], function() {
    Route::get('/userActivity', 'UserActivityController@index')->name('userActivity');
    Route::get('/userActivity/create', 'UserActivityController@create');
    Route::get('/userActivity/edit/{id}', 'UserActivityController@edit');
    Route::get('/userActivity/destroy/{id}', 'UserActivityController@destroy');
    Route::get('/userActivity/download/{id}', 'UserActivityController@download');
    Route::post('/userActivity/store', 'UserActivityController@store');
    Route::patch('/userActivity/update/{id}', 'UserActivityController@update');
    Route::get('/userActivity/logout', 'UserActivityController@logout')->name('logout');
    Route::get('/profile', 'UserActivityController@profile')->name('sProfile');
    Route::post('/profile', 'UserActivityController@profileSave');
});
