<form action="{{action('ActivityController@update', ['id' => $activity['id']])}}" method="POST">
    @csrf
    @method('PATCH')
    <h2 class="text-center font-weight-bold">Editar Atividade</h2>
    <hr>
    <div class="form-group">
        <label for="nome" class="font-weight-bold">Nome</label>
        <input type="text" class="form-control" value="{{$activity['nome']}}" name="nome" id="nome" placeholder="Nome da Atividade">
    </div>
    <button type="submit" class="btn btn-success">Salvar Atividade</button>
</form>