<form action="{{action('ActivityController@store')}}" method="POST">
    @csrf
    <h2 class="text-center font-weight-bold">Nova Atividade</h2>
    <hr>
    <div class="form-group">
        <label for="nome" class="font-weight-bold">Nome</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome da Atividade">
    </div>
    <button type="submit" class="btn btn-success">Salvar Atividade</button>
</form>