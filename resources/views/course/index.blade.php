@extends('default')
@section('content')

    <div class="container mt-5 pb-2">
        <div class="pb-3">
            <h2 class="ml-3 text-center">Cursos</h2>
            <hr>
            @if(Auth::user()['profile_id'] == 1)
                <div class="d-flex justify-content-end"><a href="{{action('CourseController@create')}}" rel="modal:open" class="btn btn-primary">Criar Curso</a></div>
            @endif
        </div>
        <div>
            <table class="table table-striped" id="tabela">
                <thead>
                    <tr>
                        <td>Nome</td>
                        <td>Código do Curso</td>
                        <td>Coordenador</td>
                        @if(Auth::user()['profile_id'] == 1)
                            <td width="20%">Ação</td>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($courses as $course)
                    <tr>
                        <td>{{$course['courseNome']}}</td>
                        <td>{{$course['cod_curso']}}</td>
                        <td>{{$course['userNome']}}</td>
                        @if(Auth::user()['profile_id'] == 1)
                            <td>
                                <a href="{{action('CourseController@edit', ['id' => $course['courseID']])}}" class="btn btn-warning btn-sm" rel="modal:open">Editar</a>
                                <a href="{{action('CourseController@destroy', ['id' => $course['courseID']])}}" class="btn btn-danger btn-sm" onclick="return confirm('Você tem certeza?')">Excluir</a>
                            </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>   
    </div>

    <script src="{{asset('js/datatable.min.js')}}"></script>
    <script src="{{asset('js/datatable.bootstrap.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#tabela').DataTable({
                
            });
        } );
    </script>

@stop