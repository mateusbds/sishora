<form action="{{action('CourseController@update', ['id' => $course['id']])}}" method="POST">
    @csrf
    @method('PATCH')
    <h2 class="text-center font-weight-bold">Novo Curso</h2>
    <hr>
    <div class="form-group">
        <label for="nome" class="font-weight-bold">Nome</label>
        <input type="text" class="form-control" value="{{$course['nome']}}" name="nome" id="nome" placeholder="Nome do Curso">
    </div>
    <div class="form-group">
        <label for="codCurso" class="font-weight-bold">Código do Curso</label>
        <input type="text" class="form-control" value="{{$course['cod_curso']}}" name="cod_curso" id="codCurso" placeholder="Código do Curso">
    </div>
    <div class="form-group">
        <label for="user_id" class="font-weight-bold">Coordenador do Curso</label>
        <select class="form-control" value="{{$course['user_id']}}" name="user_id" id="user_id">
            @foreach($users as $user)
                <option value="{{$user['id']}}">{{$user['nome']}}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-success">Salvar Curso</button>
</form>