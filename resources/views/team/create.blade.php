<form action="{{action('TeamController@store')}}" method="POST">
    @csrf
    <h2 class="text-center font-weight-bold">Cadastrar Turma</h2>
    <hr>
    <div class="form-group">
        <label for="nome" class="font-weight-bold">Nome</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome da Atividade">
    </div>
    <div class="d-flex justify-content-between mb-3">
        <div class="col-xs-5">
            <select name="from[]" id="search" class="form-control" size="8" multiple="multiple">
                @foreach($users as $user)
                    <option value="{{$user['id']}}">{{$user['nome']}}</option>
                @endforeach
            </select>
        </div>
        
        <div class="col-xs-2">
            <button type="button" id="search_rightAll" class="btn btn-block d-flex"><i data-feather="chevrons-right"></i></button>
            <button type="button" id="search_rightSelected" class="btn btn-block d-flex"><i data-feather="chevron-right"></i></button>
            <button type="button" id="search_leftSelected" class="btn btn-block d-flex"><i data-feather="chevron-left"></i></button>
            <button type="button" id="search_leftAll" class="btn btn-block d-flex"><i data-feather="chevrons-left"></i></button>
        </div>
        
        <div class="col-xs-5">
            <select name="to[]" id="search_to" class="form-control" size="8" multiple="multiple"></select>
        </div>
    </div>
    <button type="submit" class="btn btn-success">Salvar Atividade</button>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('#search').multiselect({
                search: {
                    left: '<input type="text" name="q" class="form-control" placeholder="Procurar..." />',
                    right: '<input type="text" name="q" class="form-control" placeholder="Procurar..." />',
                },
                fireSearch: function(value) {
                    return value.length > 3;
                }
            });
        });
    </script>
    <script src="{{asset('js/feather.min.js')}}"></script>
    <script>
        feather.replace();
    </script>
</form>