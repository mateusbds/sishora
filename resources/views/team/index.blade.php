@extends('default')
@section('content')

    <div class="container mt-5 pb-2">
        <div class="pb-3">
            <h2 class="ml-3 text-center">Turmas Concludentes</h2>
            <hr>
            <div class="d-flex justify-content-end"><a href="{{action('TeamController@create')}}" rel="modal:open" class="btn btn-primary">Cadastrar Turma</a></div>
        </div>
        <div>
            <table class="table table-striped" id="tabela">
                <thead>
                    <tr>
                        <td>Ano</td>
                        <td>Semestre</td>
                        <td width="20%">Ações</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($teams as $team)
                    <tr>
                        <td>{{$team['ano']}}</td>
                        <td>{{$team['semestre']}}</td>
                        <td>
                            <a href="{{action('TeamController@edit', ['id' => $team['id']])}}" class="btn btn-warning btn-sm" rel="modal:open">Editar</a>
                            <a href="{{action('TeamController@destroy', ['id' => $team['id']])}}" class="btn btn-danger btn-sm" onclick="return confirm('Você tem certeza?')">Excluir</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>   
    </div>

    <script src="{{asset('js/datatable.min.js')}}"></script>
    <script src="{{asset('js/datatable.bootstrap.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#tabela').DataTable({
                
            });
        } );
    </script>

@stop