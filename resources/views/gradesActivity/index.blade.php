@extends('default')
@section('content')

    <div class="container mt-5 pb-2">
        <div class="pb-3">
            <h2 class="ml-3 text-center">{{$grade['nome']}}</h2>
            <hr>
            <div class="d-flex justify-content-end"><a href="{{action('GradesActivityController@create', ['id' => $grade['id']])}}" rel="modal:open" class="btn btn-primary">Criar Atividade da Grade</a></div>
        </div>
        <div>
            <table class="table table-striped" id="tabela">
                <thead>
                    <tr>
                        <td>Atividade</td>
                        <td>Eixo de Atuação</td>
                        <td>Equivalência</td>
                        <td>Unidade</td>
                        <td>Limite</td>
                        <td width="20%">Ações</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($gradesActivities as $gradesActivity)
                    <tr>
                        <td>{{$gradesActivity['activityNome']}}</td>
                        <td>{{$gradesActivity['actuationNome']}}</td>
                        <td>{{$gradesActivity['quantidade']}}</td>
                        <td>{{$gradesActivity['unidade']}}</td>
                        <td>{{$gradesActivity['limite']}} horas</td>
                        <td>
                            <a href="{{action('GradesActivityController@edit', ['id' => $grade['id'], 'activityId' => $gradesActivity['gaId']])}}" class="btn btn-warning btn-sm" rel="modal:open">Editar</a>
                            <a href="{{action('GradesActivityController@destroy', ['id' => $grade['id'], 'activityId' => $gradesActivity['gaId']])}}" class="btn btn-danger btn-sm" onclick="return confirm('Você tem certeza?')">Excluir</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script src="{{asset('js/datatable.min.js')}}"></script>
    <script src="{{asset('js/datatable.bootstrap.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#tabela').DataTable({
                
            });
        } );
    </script>

@stop