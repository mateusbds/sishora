<form action="{{action('GradesActivityController@store', ['id' => $_grade->id])}}" method="POST">
    @csrf
    <h2 class="text-center font-weight-bold">Nova Atividade da Grade</h2>
    <hr>
    <div class="form-group">
        <label for="activity_id" class="font-weight-bold">Atividade</label>
        <select class="form-control" name="activity_id" id="activity_id">
            @foreach($activities as $activity)
                <option value="{{$activity['id']}}">{{$activity['nome']}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="actuation_id" class="font-weight-bold">Eixo de Atuação</label>
        <select class="form-control" name="actuation_id" id="actuation_id">
            @foreach($actuations as $actuation)
                <option value="{{$actuation['id']}}">{{$actuation['nome']}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="quantidade" class="font-weight-bold">Quantidade (inserir o % caso valor seja em porcentagem)</label>
        <input type="text" class="form-control" name="quantidade" id="quantidade" placeholder="Equivalência das horas da atividade">
    </div>
    <div class="form-group">
        <label for="limite" class="font-weight-bold">Limite de horas</label>
        <input type="text" class="form-control" name="limite" id="limite" placeholder="Limite de horas da atividade">
    </div>
    <div class="form-group">
        <label for="unidade" class="font-weight-bold">Unidade</label>
        <select class="form-control" name="unidade" id="unidade">
            <option value="DIAS">Dia</option>
            <option value="HORAS">Hora</option>
            <option value="QUANTIDADE">Unidade</option>
        </select>
    </div>
    <button type="submit" class="btn btn-success">Salvar Grade</button>
</form>