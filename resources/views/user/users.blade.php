@extends('default')
@section('content')

    <div class="container mt-4 pb-2">
        <div class="pb-3">
            <h2 class="ml-3">Usuários</h2>
            <hr>
            <div class="d-flex justify-content-end">
                <a href="{{action('UserController@create')}}" rel="modal:open" class="btn btn-primary">Criar Usuário</a>
            </div>
        </div>
        <div>
            <table class="table table-striped" id="tabela">
                <thead>
                    <tr>
                        <td>Matrícula</td>
                        <td>Nome</td>
                        <td>Email</td>
                        <td>Perfíl</td>
                        <td width="20%">Ação</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user['matricula']}}</td>
                        <td>{{$user['nome']}}</td>
                        <td>{{$user['email']}}</td>
                        <td>{{$user['profileName']}}</td>
                        <td>
                            <a href="{{action('UserController@edit', ['id' => $user['id']])}}" class="btn btn-warning btn-sm" rel="modal:open">Editar</a>
                            <a href="{{action('UserController@delete', ['id' => $user['id']])}}" class="btn btn-danger btn-sm" onclick="return confirm('Você tem certeza?')">Excluir</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>   
    </div>

    <script src="{{asset('js/datatable.min.js')}}"></script>
    <script src="{{asset('js/datatable.bootstrap.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#tabela').DataTable({
                
            });
        } );
    </script>

@stop