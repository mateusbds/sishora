@extends('default')
@section('content')

    <div class="login w-100 h-100 d-flex">
        <div class="container d-flex justify-content-center align-self-center blurred-bg">
            <form action="{{action('UserController@login')}}" method="POST" class="my-form-login d-flex flex-column align-items-center justify-content-center">
                @csrf
                <p class="my-login-title">Entrar no SisHora</p>
                <div class="input-group my-input-login">
                    <label for="matricula" class="sr-only">Matrícula</label>
                    <input name="matricula" id="matricula" class="form-control" type="text" placeholder="Matrícula">
                    <div class="input-group-append">
                        <span class="input-group-text my-bg-blue"><i class="my-login-icon" data-feather="user"></i></span>
                    </div>
                </div>
                <div class="input-group my-input-login">
                    <label for="senha" class="sr-only">Senha</label>
                    <input name="password" id="senha" class="form-control" type="password" placeholder="Senha">
                    <div class="input-group-append">
                        <span class="input-group-text my-bg-blue"><i class="my-password-icon" data-feather="lock"></i></span>
                    </div>
                </div>
                @if($errors->any())
                <div class="wrong-credentials">
                    <h6>{{$errors->all()['0']}}</h6>
                </div>
                @endif
                <div class="form-group">
                    <input class="btn btn-primary my-login-submit" type="submit" value="Entrar">
                </div>

                <div class="d-block my-recover-password"><a href="#">Esqueceu sua senha?</a></div>
            </form>
        </div>
    </div>

    <script src="{{asset('js/feather.min.js')}}"></script>
    <script>
        feather.replace();
    </script>
@stop
