@extends('default')
@section('content')

    <div class="container mt-4 mb-4">
        <h2 class="text-center">Detalhes da Atividade</h2>
        <hr>
        <div>
            <div class="m-2">
                <label for="nome" class="font-weight-bold">Nome: </label>
                {{$activity['userNome']}}
            </div>
            <div class="m-2">
                <label for="atividadeTipo" class="font-weight-bold">Tipo de Atividade: </label>
                {{$activity['activityNome']}}
            </div>
            <div class="m-2">    
                <label for="atividadeDesc" class="font-weight-bold">Descrição da Atividade: </label>
                <a class="btn btn-success btn-sm" href="{{action('UserController@viewPDF', ['id' => $activity['uaID'], 'pdf' => $activity['arquivo']])}}">Ver Atividade</a>
                <p>
                    {{$activity['uaDesc']}}
                </p>
            </div>
            <div class="m-2">
                <label for="inst" class="font-weight-bold">Instituição: </label>
                {{$activity['instituicao']}}
            </div>
            <div class="m-2">
                <label for="horas" class="font-weight-bold">Horas: </label>
                {{$activity['carga_horaria']}}
            </div>
            <div class="m-3 text-center">
                <a class="btn btn-success btn-lg" href="{{action('UserController@accept', ['id' => $activity['uaID']])}}">Aceitar</a>
                <a class="btn btn-danger btn-lg" href="{{action('UserController@refuse', ['id' => $activity['uaID']])}}">Recusar</a>
            </div>
        </div>
    </div>

@stop