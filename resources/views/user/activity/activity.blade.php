@extends('default')
@section('content')

    <div class="container mt-4 pb-2">
        <div class="pb-3">
            <h2 class="ml-3 text-center">Atividades dos Alunos</h2>
            <hr>
        </div>
        <div>
            <table class="table table-striped" id="tabela">
                <thead>
                    <tr>
                        <td>Aluno</td>
                        <td>Atividade</td>
                        <td width="20%">Ações</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($activities as $activity)
                    <tr>
                        <td>{{$activity['userNome']}}</td>
                        <td>{{$activity['descricao']}}</td>
                        <td>
                            <a href="{{action('UserController@activityView', ['id' => $activity['uaID']])}}" class="btn btn-warning btn-sm">Ver Atividade</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>   
    </div>

    <script src="{{asset('js/datatable.min.js')}}"></script>
    <script src="{{asset('js/datatable.bootstrap.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#tabela').DataTable({
                
            });
        } );
    </script>

@stop