@extends('default')
@section('content')

    <div class="login w-100 h-100 d-flex">
        <div class="container d-flex justify-content-center align-self-center blurred-bg-register">
            <form action="{{route('register')}}" method="POST" class="my-form-login d-flex flex-column align-items-center justify-content-center">
                @csrf
                <p class="my-register-title">Registrar</p>
                <div class="input-group my-input-login">
                    <label for="nome" class="sr-only">Nome</label>
                    <input name="nome" id="nome" class="form-control" type="text" placeholder="Nome">
                </div>
                <div class="input-group my-input-login">
                    <label for="email" class="sr-only">Email</label>
                    <input name="email" id="email" class="form-control" type="email" placeholder="email">
                </div>
                <div class="input-group my-input-login">
                    <label for="matricula" class="sr-only">Matrícula</label>
                    <input name="matricula" id="matricula" class="form-control" type="text" placeholder="Matrícula">
                </div>
                <div class="input-group my-input-login">
                    <label for="senha" class="sr-only">Senha</label>
                    <input name="password" id="senha" class="form-control" type="password" placeholder="Senha">
                </div>
                <div class="input-group my-input-login">
                    <label for="senha_c" class="sr-only">Repetir senha</label>
                    <input name="password_c" id="senha_c" class="form-control" type="password" placeholder="Repetir senha">
                </div>
                <div class="input-group my-input-login">
                    <label for="senha_c" class="sr-only">Repetir senha</label>
                    <select name="profile_id">
                        <option value="1">Admin</option>
                        <option value="2">Coord</option>
                        <option value="3">Aluno</option>
                    </select>
                </div>
                <div class="form-group">
                    <input class="btn btn-primary my-login-submit" type="submit" value="Entrar">
                </div>
            </form>
        </div>
    </div>

    <script src="{{asset('js/feather.min.js')}}"></script>
    <script>
        feather.replace();
    </script>
@stop
