<form action="{{action('UserController@register')}}" method="POST">
    @csrf
    <h2 class="text-center font-weight-bold">Novo Usuário</h2>
    <hr>
    <div class="form-group">
        <label for="nome" class="font-weight-bold">Nome</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome completo do usuário">
    </div>
    <div class="form-group">
        <label for="email" class="font-weight-bold">Email</label>
        <input type="email" class="form-control" name="email" id="email" placeholder="Email">
    </div>
    <div class="form-group">
        <label for="nome" class="font-weight-bold">Matrícula/SIAPE</label>
        <input type="text" class="form-control" name="matricula" id="matricula" placeholder="Matrícula ou SIAPE">
    </div>
    <div class="form-group">
        <label for="nome" class="font-weight-bold">Senha</label>
        <input type="password" class="form-control" name="password" id="senha" placeholder="Senha">
    </div>
    @if(Auth::user()['profile_id'] == '1')
        <div class="form-group">
            <label for="perfil" class="font-weight-bold">Perfíl (Apenas Administrador)</label>
            <select id="perfil" name="profile_id" name="profile_id" class="form-control">
                <option value="1">Administrador</option>
                <option value="2">Coordenador</option>
                <option value="3">Aluno</option>
            </select>
        </div>
    @endif
    <button type="submit" class="btn btn-success">Salvar Usuário</button>
</form>