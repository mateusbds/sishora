@extends('default')
@section('content')

    <div class="container mt-4 h-100 position-relative">
        <h2 class="my-index-title">Bem vindo ao SisHora, {{Auth::user()['nome']}}!</h2>
        <div class="d-flex h-100 position-absolute w-100 home-body">
            <div class="home-table align-self-center">
                Tabela
            </div>
            <div class="home-list align-self-center">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td>
                                Aluno
                            </td>
                            <td>
                                Atividade
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($activities as $activity)
                        <tr>
                            <td>
                                {{$activity['userNome']}}
                            </td>
                            <td>
                                {{$activity['activityNome']}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop