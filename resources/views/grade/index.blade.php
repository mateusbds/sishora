@extends('default')
@section('content')

    <div class="container mt-5 pb-2">
        <div class="pb-3">
            <h2 class="ml-3 text-center">Grades</h2>
            <hr>
            <div class="d-flex justify-content-end"><a href="{{action('GradeController@create')}}" rel="modal:open" class="btn btn-primary">Criar Grade</a></div>
        </div>
        <div>
            <table class="table table-striped" id="tabela">
                <thead>
                    <tr>
                        <td>Nome</td>
                        <td>Horas</td>
                        <td>Curso</td>
                        <td>Status</td>
                        <td width="30%">Ações</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($grades as $grade)
                    <tr>
                        <td>{{$grade['gradesNome']}}</td>
                        <td>{{$grade['horas']}}</td>
                        <td>{{$grade['coursesName']}}</td>
                        <td>{{$grade['status'] == 1 ? 'Ativo' : 'Depreciada'}}</td>
                        <td>
                            <a href="{{route('gradesActivity', ['id' => $grade['id']])}}" class="btn btn-primary btn-sm">Atividades da Grade</a>
                            <a href="{{action('GradeController@edit', ['id' => $grade['id']])}}" class="btn btn-warning btn-sm" rel="modal:open">Editar</a>
                            <a href="{{action('GradeController@destroy', ['id' => $grade['id']])}}" class="btn btn-danger btn-sm" onclick="return confirm('Você tem certeza?')">Excluir</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script src="{{asset('js/datatable.min.js')}}"></script>
    <script src="{{asset('js/datatable.bootstrap.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#tabela').DataTable({
                
            });
        } );
    </script>

@stop