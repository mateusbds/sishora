<form action="{{action('GradeController@update', [$grade['id']])}}" method="POST">
    @csrf
    @method('PATCH')
    <h2 class="text-center font-weight-bold">Nova Grade</h2>
    <hr>
    <div class="form-group">
        <label for="nome" class="font-weight-bold">Nome</label>
        <input type="text" class="form-control" value="{{$grade['nome']}}" name="nome" id="nome" placeholder="Nome da Grade">
    </div>
    <div class="form-group">
        <label for="horas" class="font-weight-bold">Quantidade de Horas</label>
        <input type="text" class="form-control" value="{{$grade['horas']}}" name="horas" id="horas" placeholder="Quantidade de Horas">
    </div>
    <div class="form-group">
        <label for="course_id" class="font-weight-bold">Curso</label>
        <select class="form-control" value="{{$grade['course_id']}}" name="course_id" id="course_id">
            @foreach($courses as $course)
                <option value="{{$course['id']}}">{{$course['nome']}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" name="status" id="Status" {{$grade['status'] == 1 ? 'checked' : ''}} >
        <label for="Status" class="font-weight-bold form-check-label">Ativo?</label>
    </div>
    <button type="submit" class="btn btn-success">Salvar Grade</button>
</form>