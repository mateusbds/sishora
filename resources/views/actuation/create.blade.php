<form action="{{action('ActuationController@store')}}" method="POST">
    @csrf
    <h2 class="text-center font-weight-bold">Nova Eixo de Atuação</h2>
    <hr>
    <div class="form-group">
        <label for="nome" class="font-weight-bold">Nome</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome do Eixo de Atuação">
    </div>
    <button type="submit" class="btn btn-success">Salvar Eixo de Atuação</button>
</form>