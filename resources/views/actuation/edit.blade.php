<form action="{{action('ActuationController@update', ['id' => $actuation['id']])}}" method="POST">
    @csrf
    @method('PATCH')
    <h2 class="text-center font-weight-bold">Editar Eixo de Atuação</h2>
    <hr>
    <div class="form-group">
        <label for="nome" class="font-weight-bold">Nome</label>
        <input type="text" class="form-control" value="{{$actuation['nome']}}" name="nome" id="nome" placeholder="Nome da Atividade">
    </div>
    <button type="submit" class="btn btn-success">Salvar Atividade</button>
</form>