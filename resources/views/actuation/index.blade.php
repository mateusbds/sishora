@extends('default')
@section('content')

    <div class="container mt-5 pb-2">
        <div class="pb-3">
            <h2 class="ml-3 text-center">Eixos de Atuação</h2>
            <hr>
            <div class="d-flex justify-content-end"><a href="{{action('ActuationController@create')}}" rel="modal:open" class="btn btn-primary">Criar Eixo de Atuação</a></div>
        </div>
        <div>
            <table class="table table-striped" id="tabela">
                <thead>
                    <tr>
                        <td>Nome</td>
                        <td width="20%">Ações</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($actuations as $actuation)
                    <tr>
                        <td>{{$actuation['nome']}}</td>
                        <td>
                            <a href="{{action('ActuationController@edit', ['id' => $actuation['id']])}}" class="btn btn-warning btn-sm" rel="modal:open">Editar</a>
                            <a href="{{action('ActuationController@destroy', ['id' => $actuation['id']])}}" class="btn btn-danger btn-sm" onclick="return confirm('Você tem certeza?')">Excluir</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>   
    </div>

    <script src="{{asset('js/datatable.min.js')}}"></script>
    <script src="{{asset('js/datatable.bootstrap.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#tabela').DataTable({
                
            });
        } );
    </script>

@stop