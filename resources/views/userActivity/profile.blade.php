@extends('default')
@section('content')

<div class="container mt-4 d-flex flex-column pb-3 h-100">
    <div class="profile-avatar d-flex flex-column align-items-center">
        <img src="{{$picture}}" alt="avatar">
        <input class="btn-file" type="file" value="Mudar Foto" id="file" name="avatar">
        <label for="file">Mudar Foto</label>
    </div>
    <form action="{{action('UserActivityController@profileSave')}}" method="POST" class="m-3">
        @csrf
        <div class="form-group">
            <label for="nome">Nome:</label>
            <input class="form-control" type="text" id="nome" name="nome" value="{{$user['nome']}}">
        </div>
        <div class="form-group">
            <label for="matricula">Matrícula:</label>
            <input class="form-control" type="text" id="matricula" name="matricula" value="{{$user['matricula']}}">
        </div>
        <div class="form-group">
            <label for="password-antigo">Senha Atual:</label>
            <input class="form-control" type="password" name="password_antigo" id="password_antigo">
        </div>
        <div class="form-group">
            <label for="password">Nova Senha:</label>
            <input class="form-control" type="password" name="password" id="password">
        </div>
        <div class="form-group">
            <label for="password_confirmation">Repetir Senha:</label>
            <input class="form-control" type="password" name="password_confirmation" id="password_confirmation">
        </div>
        <input class="btn btn-success" type="submit" value="Salvar">
        <input class="btn btn-danger" type="button" value="Voltar">
    </form>
</div>

<!-- @Todo colocar isso em um arquivo js separadao -->
<script>
    window.onload = function() {

        file.addEventListener('change', () => {
            let file = document.getElementById('file');

            let form = new FormData();
            form.append('avatar', file.files.item(0));
            form.append('_token', "{{csrf_token()}}");
            $.ajax({
                method: "POST",
                url: "/changePic",
                data: form,
                processData: false,
                contentType: false,
            })
            .done((msg) => {
                location.reload();
                delete file;
                delete form;
            }).fail(function(xhr, status, error) {
                console.log(xhr);
                console.log(error);
                console.log(status);
                location.reload();
                delete file;
                delete form;
            });
        });
    }
</script>

@stop