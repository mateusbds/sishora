<form action="{{action('UserActivityController@update', ['id' => $userActivity['id']])}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <h2 class="text-center font-weight-bold">Cadastrar Atividade</h2>
    <hr>
    <div class="form-group">
        <label for="descricao" class="font-weight-bold">Descrição</label>
        <input type="text" class="form-control" value="{{$userActivity['descricao']}}" name="descricao" id="descricao" placeholder="Descrição da Atividade">
    </div>
    <div class="form-group">
        <label for="carga_horaria" class="font-weight-bold">Carga Horária</label>
        <input type="text" class="form-control" value="{{$userActivity['carga_horaria']}}" name="carga_horaria" id="carga_horaria" placeholder="Quantidade de horas">
    </div>
    <div class="form-group">
        <label for="instituicao" class="font-weight-bold">Instituição</label>
        <input type="text" class="form-control" value="{{$userActivity['instituicao']}}" name="instituicao" id="instituicao" placeholder="Instituição">
    </div>
    <div class="form-group">
        <label for="grades_activity_id" class="font-weight-bold">Tipo de Atividade</label>
        <select class="form-control" value="{{$userActivity['grades_activity_id']}}" name="grades_activity_id" id="grades_activity_id">
            @foreach($gradesActivities as $gradesActivity)
            <option value="{{$gradesActivity['gradesActivitiesId']}}">
                {{$gradesActivity['nome']}}
            </option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="instituicao" class="font-weight-bold">Comprovante</label>
        <input type="file" class="form-control-file" name="arquivo" id="arquivo">
    </div>
    <button type="submit" class="btn btn-success">Salvar Atividade</button>
</form>