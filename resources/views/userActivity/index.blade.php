@extends('default')
@section('content')

    <div class="container mt-5 pb-2">
        <div class="pb-3">
            <h2 class="ml-3">Atividades</h2>
            <hr>
            <div class="d-flex justify-content-end"><a href="{{action('UserActivityController@create')}}" rel="modal:open" class="btn btn-primary">Cadastrar Atividade</a></div>
        </div>
        <div>
            <table class="table table-striped" id="tabela">
                <thead>
                    <tr>
                        <td>Descrição</td>
                        <td>Arquivo</td>
                        <td>Carga Horária</td>
                        <td>Aproveitado</td>
                        <td width="15%">Ações</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($userActivities as $userActivity)
                    <tr>
                        <td>{{$userActivity['descricao']}}</td>
                        <td><a href="{{action('UserActivityController@download', ['id' => $userActivity['id']])}}">{{$userActivity['arquivo']}}</a></td>
                        <td>{{$userActivity['carga_horaria']}}</td>
                        <td>{{$userActivity['carga_aproveitada']}}</td>
                        <td>
                            @if($userActivity['validado'] == -1)
                                <a href="{{action('UserActivityController@edit', ['id' => $userActivity['id']])}}" class="btn btn-warning btn-sm" rel="modal:open">Editar</a>
                                <a href="{{action('UserActivityController@destroy', ['id' => $userActivity['id']])}}" class="btn btn-danger btn-sm" onclick="return confirm('Você tem certeza?')">Excluir</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>   
    </div>

    <script src="{{asset('js/datatable.min.js')}}"></script>
    <script src="{{asset('js/datatable.bootstrap.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#tabela').DataTable({
                
            });
        } );
    </script>

@stop