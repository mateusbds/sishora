
        @if(!Request::is('login'))
            <footer class="footer">
                Todos os direitos reservados.
            </footer>
        @endif
        <script src="{{asset('js/popper.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/multiselect.min.js')}}"></script>
        <script src="{{asset('js/modal.min.js')}}"></script>
        <script src="{{asset('js/datatable.min.js')}}"></script>
        <script src="{{asset('js/datatable.bootstrap.min.js')}}"></script>
    </body>
</html>