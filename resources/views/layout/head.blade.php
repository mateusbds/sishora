<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>SisHora</title>

        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/modal.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/datatable.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/style.css')}}">

        <script src="{{asset('js/jquery.min.js')}}"></script>        

    </head>
    <body>