
@if(Auth::check() && Auth::user())
  <div class="w-100">
    <nav class="navbar navbar-expand-lg my-navbar-light my-navbar">
        <a href="#" class="navbar-brand">SISHORA</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar">
          <span class="navbar-toggler-icon"></span>
        </button>
  
        <div class="collapse navbar-collapse" id="navbar">
          <ul class="navbar-nav w-100">
            <li class="nav-item mr-2 {{Request::is('admin') ? 'active' : ''}}">
              <a class="nav-link white-link d-flex justify-content-center align-items-center" href="{{Auth::user()['profile_id'] == 3 ? action('UserActivityController@index') : action('UserController@index')}}">
                  <i class="my-menu-icon mr-1" data-feather="home"></i>Inicio<span class="sr-only">(current)</span>
              </a>
            </li>
            @if(Auth::user()['profile_id'] != 3)
              <li class="nav-item mr-2 d-flex justify-content-center align-items-center dropdown {{(Request::is('admin/user') || Request::is('admin/user/activities')) ? 'active' : ''}}">
                <a class="nav-link dropdown-toggle white-link d-flex justify-content-center align-items-center" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="my-menu-icon mr-1" data-feather="users"></i>
                  Alunos
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{action('UserController@users')}}">Listar Alunos</a>
                  <a class="dropdown-item" href="{{action('UserController@activities')}}">Listar Atividades</a>
                </div>
              </li>
              <li class="nav-item mr-2 {{Request::is('admin/course') ? 'active' : ''}}">
                <a class="nav-link white-link d-flex justify-content-center align-items-center" href="{{action('CourseController@index')}}">
                  <i class="my-menu-icon mr-1" data-feather="book"></i>
                  Cursos
                </a>
              </li>
              <li class="nav-item mr-2 d-flex justify-content-center align-items-center {{(Request::is('admin/grade') || Request::is('admin/grade/'.(isset($gradesActivity['id']) ? $gradesActivity['id'] : '').'/gradesActivity')) ? 'active' : ''}}">
                <a class="nav-link white-link d-flex justify-content-center align-items-center" href="{{action('GradeController@index')}}">
                  <i class="my-menu-icon mr-1" data-feather="file-text"></i>
                  Grades
                </a>
              </li>
              <li class="nav-item mr-2 {{Request::is('admin/activity') ? 'active' : ''}}">
                <a class="nav-link white-link d-flex justify-content-center align-items-center" href="{{action('ActivityController@index')}}">
                  <i class="my-menu-icon mr-1" data-feather="clock"></i>
                  Atividades Complementares
                </a>
              </li>
              <li class="nav-item mr-2 d-flex justify-content-center align-items-center {{Request::is('admin/actuation') ? 'active' : ''}}">
                <a class="nav-link white-link d-flex justify-content-center align-items-center" href="{{action('ActuationController@index')}}">
                  <i class="my-menu-icon mr-1" data-feather="share-2"></i>
                  Eixos de Atuação
                </a>
              </li>
              <li class="nav-item mr-2 {{Request::is('admin/team') ? 'active' : ''}}">
                <a class="nav-link white-link d-flex justify-content-center align-items-center" href="{{action('TeamController@index')}}">
                  <i class="my-menu-icon mr-1" data-feather="menu"></i>
                  Turmas
                </a>
              </li>
            @endif
            <li class="nav-item dropdown ml-auto d-flex flex-row align-items-center">
              <i class="my-menu-icon" data-feather="bell"></i>
              <div class="d-inline-block my-profile-pic"><img src="{{Storage::url('public/profile_pictures/'. (Auth::user()['avatar'] != null ? Auth::user()['avatar'] : 'gravatar.jpg')) }}" alt="avatar"></div>
              <a href="#" class="nav-link dropdown-toggle white-link" id="dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{explode(" ", Auth::user()['nome'], 2)[0]}}</a>
              <div class="dropdown-menu" aria-labelledby="dropdown">
                @if(Auth::user()['profile_id'] != 3)
                  <a class="dropdown-item m-menu" href="{{action('UserController@profile')}}">Editar Perfil</a>
                  <a class="dropdown-item m-menu" href="{{action('UserController@logout')}}">Sair</a>
                @else
                  <a class="dropdown-item m-menu" href="{{action('UserActivityController@profile')}}">Editar Perfil</a>
                  <a class="dropdown-item m-menu" href="{{action('UserActivityController@logout')}}">Sair</a>
                @endif
              </div>
            </li>
          </ul>
      </div>
      
    </nav>
  </div>
  @if(!empty($errors->all()))
  <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
  </div>
  @endif
  @if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
  @endif
  <script src="{{asset('js/feather.min.js')}}"></script>
  <script>
      feather.replace();
  </script>
@endif
